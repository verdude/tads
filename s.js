const keyGrabber = (function() {
  let button;

  function validateKeys(id, secret) {
    const ID_LENGTH = 20;
    const SECRET_LENGTH = 40;

    console.debug(id, secret);

    if (!id || !secret) {
      console.error("ID/Secret is empty.");
      return false;
    }

    if (id.length != ID_LENGTH) {
      console.error(`Invalid ID Length: ${id}`);
      return false;
    }

    if (secret.length != SECRET_LENGTH) {
      console.error(`Invalid SECRET Length: ${secret}`);
      return false;
    }

    return true;
  }

  function addButton() {
    if (button) return;

    const body = document.querySelector("body");
    button = document.createElement("button");
    button.textContent = "Save Keys";
    button.addEventListener("click", run);
    button.style.position = "absolute";
    button.style.top = "2em";
    button.style.left = "2em";
    button.style.margin = ".25em";
    button.style.padding = ".2em";
    button.style.color = "#D4F1F4";
    button.style["background-color"] = "#05445E";

    body.addChild(button);
  }

  function findKeys() {
    const inputs = [].map.call(document.querySelectorAll("input"), e => e.value);
    return inputs.slice(-2);
  }

  function sendKeys() {
    if (!button) {
      addButton();
    }
    button.textContent = "Sending Keys...";
  }

  function run(id, secret) {
    const [id, secret] = findKeys();
    if (validateKeys(id, secret)) {
      console.log("Got keys");
      sendKeys(id, secret);
    } else {
      addButton();
    }
  }

  return {
    run: run
  };
}());

keyGrabber.run();
